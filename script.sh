clear
make all -s
echo 'Le programme commence'
echo ''
if [[ "$1" == "--help" ]]; then
arguments="--help"
./exe $arguments
exit 1
fi
#DATASETS
start_time=$(date +%s.%N)
echo '1 - On vérifie la présence des datasets'
echo ''
./decoupage
if [ $? -eq 0 ]
then
echo '    Les datasets sont prêts'
echo ''
else
echo '    Il y a eu un problème avec les datasets'
echo ''
echo '    ARRET'
exit 1
fi
end_time=$(date +%s.%N)
elapsed_time=$(echo "$end_time - $start_time" | bc)
printf "    Le temps écoulé pour les datasets est de %.1f secondes.\n" $elapsed_time
echo ''
#FIN DATASETS

#DEBUT DU C
echo '2 - Le programme C commence'
echo ''
if [[ "$1" == "" ]]; then
arguments="-i assets/meteo_filtered_data_v1.csv"
else
case $1 in
    "-F") arguments="-i assets/dataset_france.csv "
    ;;
    "-G") arguments="-i assets/dataset_guyane.csv "
    ;;
    "-S") arguments="-i assets/dataset_stPierreEtMiquelon.csv "
    ;;
    "-A") arguments="-i assets/dataset_antilles.csv "
    ;;
    "-O") arguments="-i assets/dataset_oceanIndien.csv "
    ;;
    "-Q") arguments="-i assets/dataset_antartique.csv "
    ;;
    *) exit 1
    ;;
esac
fi
for i in $(seq 2 $#)
do
  if [ ${!i} = "-o" ]
  then
  let i++
  output="${!i}"
  let i--
  fi
  arguments+="${!i} " #concaténation des arg
done
#ARGUMENTS DU C
echo '    Le programme est en cours'
echo '    .............................'
echo ''
if [ $# -eq 0 ]
then
echo 'No arguments'
else
./exe $arguments
fi
if [ $? -eq 2 ]
then
  echo ''
  echo 'ARGUMENTS EXCLUSIF UTILISES AVEC D AUTRES'
elif [ $? -eq 3 ]
then
  echo ''
  echo 'PAS ASSEZ DARGUMENTS'
fi
end_time=$(date +%s.%N)
eval "gnuplot plot.gp"
eval "display png/$output.png"
elapsed_time=$(echo "$end_time - $start_time" | bc)
printf "    Le temps écoulé pour le programme au total est de %.1f secondes.\n" $elapsed_time
make clean -s #supprime les executables et les fichiers temporaires pour plus de lisibilité