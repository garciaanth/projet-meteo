set datafile separator ';'
set title 'temp mode1 en mode barre d'erreur'
set xlabel ' ID station'
set ylabel 'temperatrure'
set autoscale x
set autoscale y
set yrange [-40:60]
set errorbars linecolor black linewidth 0.5 dashtype '.'
set terminal png
set output 'temperature1.png'
plot 'NOM_FICHIER' using 1:4:3:2 w yerrorbars