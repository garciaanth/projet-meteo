set datafile separator ";"
set title textcolor "red"
set title "graphique humidité"
set xlabel "longitude"
set ylabel "latitude"
set pm3d
set dgrid3d 100,100,3
set autoscale x
set autoscale y
set terminal png
set output "png/humidite.png"
splot "out/humidite.csv" u 1:2:3 w pm3d