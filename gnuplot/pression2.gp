set datafile separator ";"
set xdata time
set timefmt "%Y:%m"
set xrange ["2010:1":"2022:9"]
set format x "%Y:%m"
set terminal png
set output "pression2.png"
plot 'NOMFICHIERSORTIE' using 1:2 w linespoints