set datafile separator ";"
set title textcolor "blue"
set title "graphique hauteur"
set xlabel "longitude"
set ylabel "latitude"
set pm3d
set dgrid3d 300,300,3
set autoscale x
set autoscale y
set xtics add
set palette defined (0 "green", 200 "dark-green", 450 "dark-olivegreen",650 "dark-goldenrod", 900 "brown")
set terminal png
set output "png/hauteur.png"
splot "out/hauteur.csv" u 1:2:3 w pm3d