set datafile separator ";"
set title "pression mode1 en mode barre d'erreur"
set xlabel " ID station"
set ylabel "pression"
set autoscale x
set autoscale y
set errorbars linecolor black linewidth 0.5 dashtype '.'
set terminal png
set output "pression1.png"
plot 'NOM_FICHIER' using 1:4:3:2 w yerrorbars