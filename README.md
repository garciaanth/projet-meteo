# Projet météo

Ce projet avait pour but de créer un programme permettant de sortir des graphiques représentants les données météorologiques des stations françaises et autre (Guyane,   Antarctique, etc...). Nous avons donc créer un programme en C qui est capable d'aller rechercher les éléments qui l'intéresse grâce a des arguments saisis par l'utilisateur  et sortir un fichier csv qui est récuperer par gnuplot permettant de créer des graphiques associé directement aux données sortie. Le tout est géré par un script qui récupère  les arguments de l'endroit géographique et des options souhaité par l'utilisateur, en suivant le script appelle le programme en C est récupère le .csv en sortie  pour le donner a gnuplot.



## Problèmes Connus

Pour utiliser -g ou -a il faut mettre un "/" avant vos chiffres  
Les arguments du script ne fonctionnent que si ils sont en première position  
La température et la pression mode 3 ne fonctionnent pas  
Si le fichier entrée est un .csv qui contient une première ligne explicative pour les colonnes, le programme générera un segfault  


## Script


Le script appelle un premier programme en C qui vérifie si les datasets découpés sont présents.  
Si les datasets sont présents => le .c revoie 0 et le script fait le traitement  
Si les datasets ne sont pas présents => le .c les crée, les complètes et renvoie 0  



## Executable

==>Decoupage :  
Ce programme découpe le dataset d'origine en plusieurs dataset (un par pays) selon les ID de stations  

[REGLES DE DECOUPAGE]  
ID >= 7005 et ID <= 7790 -> FRANCE  
ID >= 81401 et ID <= 81415 -> GUYANE  
ID >= 61968 et ID <= 61996 -> OCEAN INDIEN  
ID = 61988 ou ID = 61996 -> ANTARTIQUE  
ID = 71805 -> ST PIERRE ET MIQUELON  
ID >= 78890 et ID <= 78925 -> ANTILLE  

Pour Executer le programme plusieurs librairies sont necessaires  
  
-gnuplot (http://www.gnuplot.info/download.html)  
  
-gcc (https://gcc.gnu.org/install/download.html)  

-imagemagick (https://imagemagick.org/)  



## Aide

Pour connaitre les différent arguments disponible, il suffit d'appeller le script avec l'argument  
--help  