# Définir la variable CC
CC=gcc
# Vérifier le système d'exploitation
ifeq ($(OS),Windows_NT)
    CC=gcc # Windows utilise gcc par défaut
else
    UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Darwin)
        CC=gcc-12 # MacOS utilise gcc-12
    endif
    ifeq ($(UNAME_S),Linux)
        CC=gcc # Ubuntu utilise gcc-9
    endif
endif

HEADER_FILES := $(wildcard headers/*.h) #Réunis tous les .h

all : hauteur humidite exe temperature1 temperature2 temperature3 pression1 pression2 pression3 vent decoupage
	
exe : temp/main.o 
	$(CC) temp/main.o temp/meteo.o -o exe
	
temp/main.o : src/main.c $(HEADER_FILES)
	$(CC) -Wall -c src/main.c -o temp/main.o

temp/meteo.o : src/meteo.c $(HEADER_FILES)
	$(CC) -Wall -c src/meteo.c -o temp/meteo.o

clean :
	find temp/ -type f ! -name '.gitkeep' -delete
	rm decoupage hauteur humidite exe temperature1 temperature2 temperature3 pression1 pression2 pression3 vent plot.gp

temp/decoupage.o : src/decoupage.c $(HEADER_FILES)
	$(CC) -Wall -c src/decoupage.c -o temp/decoupage.o

decoupage : temp/decoupage.o temp/meteo.o
	$(CC) temp/decoupage.o temp/meteo.o -o decoupage

temp/humidite.o : src/humidite.c $(HEADER_FILES)
	$(CC) -Wall -c src/humidite.c -o temp/humidite.o

humidite : temp/humidite.o temp/meteo.o
	$(CC) -Wall temp/humidite.o temp/meteo.o -o humidite

temp/hauteur.o : src/hauteur.c $(HEADER_FILES)
	$(CC) -Wall -c src/hauteur.c -o temp/hauteur.o

hauteur : temp/hauteur.o temp/meteo.o
	$(CC) temp/hauteur.o temp/meteo.o -o hauteur

temp/vent.o : src/vent.c $(HEADER_FILES)
	$(CC) -Wall -c src/vent.c -o temp/vent.o -lm

vent : temp/vent.o temp/meteo.o
	$(CC) temp/vent.o temp/meteo.o -o vent -lm

temperature : temp/temperature.o temp/meteo.o
	$(CC) temp/temperature.o temp/meteo.o -o temperature -lm

temp/temperature.o : src/temperature.c $(HEADER_FILES)
	$(CC) -Wall -c src/temperature.c -o temp/temperature.o -lm

pression : temp/pression.o temp/meteo.o
	$(CC) temp/pression.o temp/meteo.o -o pression -lm

temp/pression.o : src/pression.c $(HEADER_FILES)
	$(CC) -Wall -c src/pression.c -o temp/pression.o -lm

temp/temperature2.o : src/temperature2.c $(HEADER_FILES)
	$(CC) -Wall -c src/temperature2.c -o temp/temperature2.o -lm

temperature2 : temp/temperature2.o temp/meteo.o
	$(CC) temp/temperature2.o temp/meteo.o -o temperature2 -lm

temp/temperature1.o : src/temperature1.c $(HEADER_FILES)
	$(CC) -Wall -c src/temperature1.c -o temp/temperature1.o -lm

temperature1 : temp/temperature1.o temp/meteo.o
	$(CC) temp/temperature1.o temp/meteo.o -o temperature1 -lm

temp/temperature3.o : src/temperature3.c $(HEADER_FILES)
	$(CC) -Wall -c src/temperature3.c -o temp/temperature3.o -lm

temperature3 : temp/temperature3.o temp/meteo.o
	$(CC) temp/temperature3.o temp/meteo.o -o temperature3 -lm

temp/pression1.o : src/pression1.c $(HEADER_FILES)
	$(CC) -Wall -c src/pression1.c -o temp/pression1.o -lm

pression1 : temp/pression1.o temp/meteo.o
	$(CC) temp/pression1.o temp/meteo.o -o pression1 -lm

temp/pression2.o : src/pression2.c $(HEADER_FILES)
	$(CC) -Wall -c src/pression2.c -o temp/pression2.o -lm

pression2 : temp/pression2.o temp/meteo.o
	$(CC) temp/pression2.o temp/meteo.o -o pression2 -lm

temp/pression3.o : src/pression3.c $(HEADER_FILES)
	$(CC) -Wall -c src/pression3.c -o temp/pression3.o -lm

pression3 : temp/pression3.o temp/meteo.o
	$(CC) temp/pression3.o temp/meteo.o -o pression3 -lm
