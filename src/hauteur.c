#include "../headers/meteo.h"
#include "../headers/hauteur.h"

int main(int argc, char *argv[])
{
  arbreID arbreTest;
  FILE* fluxFrance = NULL;
  struct bornes valBornes;
  valBornes.dateMin.annee = atoi(argv[1]);
  valBornes.dateMin.mois = atoi(argv[2]);
  valBornes.dateMin.jour = atoi(argv[3]);
  valBornes.dateMax.annee = atoi(argv[4]);
  valBornes.dateMax.mois = atoi(argv[5]);
  valBornes.dateMax.jour = atoi(argv[6]);
  valBornes.latMin = atof(argv[7]);
  valBornes.latMax = atof(argv[8]);
  valBornes.longMin = atof(argv[9]);
  valBornes.longMax = atof(argv[10]);
  char* input = NULL;
  char* output = NULL;
  input = argv[11];
  output = argv[12];
  fluxFrance = fopen(input,"r");
  arbreTest = lectureDataSet(fluxFrance,valBornes);
  fclose(fluxFrance);
  FILE* fluxSortie = NULL;
  char nomFichierSortieDat[1000];
  strcpy(nomFichierSortieDat,"out/");
  strcat(nomFichierSortieDat,output);
  strcat(nomFichierSortieDat,".dat");
  fluxSortie = fopen(nomFichierSortieDat,"w");
  ecrireInfixeID(arbreTest,fluxSortie);
  fclose(fluxSortie);
  ecrireGNUPLOT(output);
  return 0;
}

arbreID lectureDataSet(FILE* fluxSource,struct bornes valBornes)
{
arbreID arbreID = NULL;
char ligne[BUFFER_SIZE];
char* laLigne;
point pointActuel;
char* token;
if (fluxSource != NULL)
{
  while (fgets(ligne,BUFFER_SIZE,fluxSource) != NULL)
  {
      laLigne = ligne;
      while (laLigne != NULL)
      {
      token = strsep(&laLigne,";");
      pointActuel.ID = atoi(token);
      token = strsep(&laLigne,";");
      pointActuel.date = decoupageDate(token);
      token = strsep(&laLigne,";");
      pointActuel.pressionNiveauMer = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.directionVent = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.vitesseVent = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.humidite = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.pressionStation = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.variationPression = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.precipitations = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.coordonnees = decoupageCoordonnees(token);
      token = strsep(&laLigne,";");
      pointActuel.temperature = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.temperatureMin = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.temperatureMax = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.altitude = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.codeCommune = token;
      }
      if (dansLesBornes(valBornes,pointActuel) == 0)
      {
        arbreID = insertionArbreID(pointActuel, arbreID);
      }
    }
  }
  return arbreID;
} 

arbreID insertionArbreID(point valeur,arbreID a){
  if (a == NULL)
  {
    a = creerArbreID(valeur.ID,valeur.altitude,valeur.coordonnees.latitude,valeur.coordonnees.longitude,NULL,NULL);
  } else if (valeur.ID > a->valeur.ID)
    {
      a->filsD = (insertionArbreID(valeur,a->filsD));
    } else if (valeur.ID < a->valeur.ID) 
    {
      a->filsG = (insertionArbreID(valeur,a->filsG));
    }
  return(a);
}

arbreID creerArbreID(int ID,float altitude, float lat, float longi,arbreID filsG,arbreID filsD){
  arbreID a;
  a = malloc(sizeof(noeudID));
  a->valeur.ID = ID;
  a->valeur.altitude = altitude;
  a->valeur.latitude = lat;
  a->valeur.longitude = longi;
  a->filsG = filsG;
  a->filsD = filsD;
  return(a);
}

void afficherInfixeID(arbreID a){
  if (a == NULL) {
    printf("\n");
  } else {
    afficherInfixeID(a->filsG);
    printf("%d | ",a->valeur.ID);
    printf("%f",a->valeur.altitude);
    afficherInfixeID(a->filsD);

  }
}

void ecrireInfixeID(arbreID a,FILE* fluxSortie){
    if (a == NULL) {
  } else {
    ecrireInfixeID(a->filsG,fluxSortie);
    fprintf(fluxSortie,"%f;",a->valeur.longitude);
    fprintf(fluxSortie,"%f;",a->valeur.latitude);
    fprintf(fluxSortie,"%f;\n",a->valeur.altitude);
    ecrireInfixeID(a->filsD,fluxSortie);
}
}



int dansLesBornes(struct bornes valBornes,point valeur){
  int res = 1;
  if ((valeur.date.annee <= valBornes.dateMax.annee) && (valeur.date.annee >= valBornes.dateMin.annee))
  {
    if (valeur.date.annee == valBornes.dateMax.annee && valeur.date.mois < valBornes.dateMin.mois)
    {
      return(res);
    }
    if (valeur.date.annee == valBornes.dateMax.annee && valeur.date.mois > valBornes.dateMax.mois)
    {
      return(res);
    }
    if (valeur.date.mois >= valBornes.dateMin.mois && valeur.date.mois <= valBornes.dateMax.mois)
    {
      if (valeur.date.mois == valBornes.dateMin.mois && valeur.date.jour < valBornes.dateMin.jour)
      {
        return(res);
      }
      if (valeur.date.mois == valBornes.dateMax.mois && valeur.date.jour > valBornes.dateMax.jour)
      {
        return(res);
      }
      if (valeur.date.jour >= valBornes.dateMin.jour && valeur.date.jour <= valBornes.dateMax.jour)
      {
        if ((valeur.coordonnees.latitude >= valBornes.latMin) && (valeur.coordonnees.latitude <= valBornes.latMax) &&
            (valeur.coordonnees.longitude >= valBornes.longMin) && (valeur.coordonnees.longitude <= valBornes.longMax))
        {
          res = 0;
        }
      }
    }
  }
  return(res);
}

void ecrireGNUPLOT(char* output){
  FILE *fichierGP = fopen("plot.gp", "w");
  fprintf(fichierGP,"set datafile separator ';'\n");  
  fprintf(fichierGP,"set title textcolor 'blue'\n");
  fprintf(fichierGP,"set title 'graphique hauteur'\n");
  fprintf(fichierGP,"set xlabel 'longitude'\n");  
  fprintf(fichierGP,"set ylabel 'latitude'\n");
  fprintf(fichierGP,"set pm3d\n");
  fprintf(fichierGP,"set dgrid3d 300,300,3\n");
  fprintf(fichierGP,"set xtics add\n");  
  fprintf(fichierGP,"set autoscale x\n");
  fprintf(fichierGP,"set autoscale y\n");
  fprintf(fichierGP,"set palette defined (0 'green', 200 'dark-green', 450 'dark-olivegreen',650 'dark-goldenrod', 900 'brown')\n");
  fprintf(fichierGP,"set terminal png\n");
  fprintf(fichierGP,"set output 'png/%s.png'\n",output);
  fprintf(fichierGP,"splot 'out/%s.dat' u 1:2:3 w pm3d \n",output);
  fclose(fichierGP);
}
