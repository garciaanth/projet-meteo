#include "../headers/meteo.h"

/*
Liste des erreurs :
Pas d'arguments : 1
Arguments exclusifs utilisés avec d'autres : 2
Pas assez d'arguments : 3
*/ 


/*
Temperature = 1
Pression = 2
Vent = 3
Hauteur = 4
Humidite = 5

*/
int main(int argc, char *argv[]) {
    int c;
    int flag_optionExclusive = 0; //Permet de vérifier si on utilise + de 1 arg exclusif
    int flag_optionObligatoire = 0; //Permet de vérifier si les arg oblig sont la (i o + exclusif)
    struct date datesMin;
    struct date datesMax;
    float latitudeMin = -100;
    float latitudeMax = 100;
    float longitudeMin = -100;
    float longitudeMax = 100;
    char* nomFichierSource = NULL;
    char* nomFichierSortie = NULL;
    int mode = 0;
    char commande[150];
    struct option long_options[] =
    {
      {"help", 0, 0, 'x'},
      {"temperatures",1,0,'t'},
      {"pressions_atmospheriques",1,0,'p'},
      {"vent",0,0,'w'},
      {"hauteur",0,0,'h'},
      {"humidite",0,0,'m'},
      {"longitude",2,0,'g'},
      {"latitude",2,0,'a'},
      {"dates",6,0,'d'},
      {"input",1,0,'i'},
      {"output", 1, 0, 'o'},
      {0, 0, 0, 0}
    };
    int tab_arguments[argc]; //Tableau contenant tous les arguments
    int indice_tab_arguments = 0;
    int indice_tab_argv = indice_tab_arguments + 1;
    float tmp; //IL FAUT QU'IL SOIT LA DEMANDEZ PAS
    
    // Initialisation des min max pour les dates
    datesMin.annee = 0;
    datesMin.mois = 0;
    datesMin.jour = 0;
    datesMax.annee = 2050;
    datesMax.mois = 13;
    datesMax.jour = 13;
    
    // Boucle de traitement des options
    while ((c = getopt_long(argc, argv,"xt:p:whmg:a:d:i:o:",long_options,NULL)) != -1)
    {
        switch (c)
        {
            case 'x':
                //le --help
                if (flag_optionExclusive != 0) //Vérification si arg exclusif déjà utilisé
                {
                    return(2);
                } else {
                tab_arguments[indice_tab_arguments] = 0;
                flag_optionExclusive = 1; //Pour empecher un nouvel arg exclusif
                flag_optionObligatoire = 3;
                indice_tab_arguments++;
                indice_tab_argv = indice_tab_arguments + 1;
                break;
                }
            case 't':
                //Temperatures + demande le mode
                if (flag_optionExclusive != 0) //Vérification si arg exclusif déjà utilisé
                {
                    return(2);
                } else {
                tab_arguments[indice_tab_arguments] = 1;
                indice_tab_arguments++;
                indice_tab_argv++;
                mode = atoi(optarg);
                flag_optionExclusive = 1; //Pour empecher un nouvel arg exclusif
                flag_optionObligatoire++;
                break;
                }
            case 'p':
                //Pression atmospherique + demande le mode
                if (flag_optionExclusive != 0) //Vérification si arg exclusif déjà utilisé
                {
                    return(2);
                } else {
                tab_arguments[indice_tab_arguments] = 2;
                indice_tab_arguments++;
                indice_tab_argv++;
                mode = atoi(optarg);
                flag_optionExclusive = 1; //Pour empecher un nouvel arg exclusif
                flag_optionObligatoire++;
                break;}
            case 'w':
                //Vent
                if (flag_optionExclusive != 0) //Vérification si arg exclusif déjà utilisé
                {
                    return(2);
                } else {
                tab_arguments[indice_tab_arguments] = 3;
                flag_optionExclusive = 1; //Pour empecher un nouvel arg exclusif
                flag_optionObligatoire++;
                indice_tab_arguments++;
                indice_tab_argv = indice_tab_arguments + 1;
                break;}
            case 'h':
                //Hauteur
                if (flag_optionExclusive != 0) //Vérification si arg exclusif déjà utilisé
                {
                    return(2);
                } else {
                tab_arguments[indice_tab_arguments] = 4;
                flag_optionExclusive = 1; //Pour empecher un nouvel arg exclusif
                flag_optionObligatoire++;
                indice_tab_arguments++;
                indice_tab_argv = indice_tab_arguments + 1;
                break;}
            case 'm':
                //Humidité
                if (flag_optionExclusive != 0) //Vérification si arg exclusif déjà utilisé
                {
                    return(2);
                } else {
                tab_arguments[indice_tab_arguments] = 5;
                flag_optionExclusive = 1; //Pour empecher un nouvel arg exclusif
                flag_optionObligatoire++;
                indice_tab_arguments++;
                indice_tab_argv = indice_tab_arguments + 1;
                break;
                }
            case 'g':
                //Longitude + min max
                sscanf(optarg,"/%f",&tmp);
                longitudeMin=tmp;
                sscanf(argv[optind],"/%f",&tmp);
                longitudeMax=tmp;
                break;
            case 'a':
                //Latitude + min max
                sscanf(optarg,"/%f",&tmp);
                latitudeMin=tmp;
                sscanf(argv[optind],"/%f",&tmp);
                latitudeMax=tmp;
                break;
            case 'd':
                //Dates + min max
                datesMin.annee = atoi(argv[optind]);
                datesMin.mois = atoi(argv[optind+1]);
                datesMin.jour = atoi(argv[optind+2]);

                datesMax.annee = atoi(argv[optind+3]);
                datesMax.mois = atoi(argv[optind+4]);
                datesMax.jour = atoi(argv[optind+5]);
                break;
            case 'i':
                //Fichier source + nom ficher
                nomFichierSource = optarg;
                flag_optionObligatoire++;
                break;
            case 'o':
                //Fichier sortie + nom fichier sortie
                nomFichierSortie = optarg;
                flag_optionObligatoire++;
                break;
            case '?':
                //On sait pas ce que c'est
                break;
        }
    }
        if (flag_optionObligatoire < 3)
    {
        printf("Pas assez d'arugments\n");
        return(3);
    } else {
    for (int indiceActuel = 0; indiceActuel < indice_tab_arguments; indiceActuel++)
    {
        switch (tab_arguments[indiceActuel])
        {
        case 0:
            //Appeller le help
            help_c();
            return(15);
            break;
        case 1:
            //Apeller temperature(mode,datesMin,datesMax,latMin,latMax,longMin,longMax,input,output)
            if (mode == 1)
            {
            printf("    Vous avez choisi l'option Température mode %d\n",mode);
            sprintf(commande, "./temperature1 %d %d %d %d %d %d %f %f %f %f %s %s",datesMin.annee,datesMin.mois,datesMin.jour,datesMax.annee,datesMax.mois,datesMax.jour,latitudeMin,latitudeMax,longitudeMin,longitudeMax,nomFichierSource,nomFichierSortie);
            return (system(commande));
            } else if (mode == 2)
            {
            printf("    Vous avez choisi l'option Température mode %d\n",mode);
            sprintf(commande, "./temperature2 %d %d %d %d %d %d %f %f %f %f %s %s",datesMin.annee,datesMin.mois,datesMin.jour,datesMax.annee,datesMax.mois,datesMax.jour,latitudeMin,latitudeMax,longitudeMin,longitudeMax,nomFichierSource,nomFichierSortie);
            return (system(commande));
            } else if (mode == 3)
            {
            printf("    Vous avez choisi l'option Température mode %d\n",mode);
            sprintf(commande, "./temperature3 %d %d %d %d %d %d %f %f %f %f %s %s",datesMin.annee,datesMin.mois,datesMin.jour,datesMax.annee,datesMax.mois,datesMax.jour,latitudeMin,latitudeMax,longitudeMin,longitudeMax,nomFichierSource,nomFichierSortie);
            return (system(commande));
            } else {
                return(10);
            }
            break;
        case 2:
            //Apeller pression(indiceActuel+1,datesMin,datesMax,latMin,latMax,longMin,longMax,input,output)
            if (mode == 1)
            {
            printf("    Vous avez choisi l'option Pression mode %d\n",mode);
            sprintf(commande, "./pression1 %d %d %d %d %d %d %f %f %f %f %s %s",datesMin.annee,datesMin.mois,datesMin.jour,datesMax.annee,datesMax.mois,datesMax.jour,latitudeMin,latitudeMax,longitudeMin,longitudeMax,nomFichierSource,nomFichierSortie);
            return (system(commande));
            } else if (mode == 2)
            {
            printf("    Vous avez choisi l'option Pression mode %d\n",mode);
            sprintf(commande, "./pression2 %d %d %d %d %d %d %f %f %f %f %s %s",datesMin.annee,datesMin.mois,datesMin.jour,datesMax.annee,datesMax.mois,datesMax.jour,latitudeMin,latitudeMax,longitudeMin,longitudeMax,nomFichierSource,nomFichierSortie);
            return (system(commande));
            } else if (mode == 3)
            {
            printf("    Vous avez choisi l'option Pression mode %d\n",mode);
            sprintf(commande, "./pression3 %d %d %d %d %d %d %f %f %f %f %s %s",datesMin.annee,datesMin.mois,datesMin.jour,datesMax.annee,datesMax.mois,datesMax.jour,latitudeMin,latitudeMax,longitudeMin,longitudeMax,nomFichierSource,nomFichierSortie);
            return (system(commande));
            } else {
                return(10);
            }
            break;
        case 3:
            //Apeller Vent(datesMin,datesMax,latMin,latMax,longMin,longMax,input,output)
            printf("    Vous avez choisi l'option vent\n");
            sprintf(commande, "./vent %d %d %d %d %d %d %f %f %f %f %s %s",datesMin.annee,datesMin.mois,datesMin.jour,datesMax.annee,datesMax.mois,datesMax.jour,latitudeMin,latitudeMax,longitudeMin,longitudeMax,nomFichierSource,nomFichierSortie);
            return (system(commande));
            break;
        case 4:
            //Apeller Hauteur(datesMin,datesMax,latMin,latMax,longMin,longMax,input,output)
            printf("    Vous avez choisi l'option Hauteur\n");
            sprintf(commande, "./hauteur %d %d %d %d %d %d %f %f %f %f %s %s",datesMin.annee,datesMin.mois,datesMin.jour,datesMax.annee,datesMax.mois,datesMax.jour,latitudeMin,latitudeMax,longitudeMin,longitudeMax,nomFichierSource,nomFichierSortie);
            return (system(commande));
            break;
        case 5:
            //Apeller Humidité(datesMin,datesMax,latMin,latMax,longMin,longMax,input,output)
            printf("    Vous avez choisi l'option Humidité\n");
            sprintf(commande, "./humidite %d %d %d %d %d %d %f %f %f %f %s %s",datesMin.annee,datesMin.mois,datesMin.jour,datesMax.annee,datesMax.mois,datesMax.jour,latitudeMin,latitudeMax,longitudeMin,longitudeMax,nomFichierSource,nomFichierSortie);
            return (system(commande));
            break;
        default:
            break;
        }
    }
    }
  return 0;
}