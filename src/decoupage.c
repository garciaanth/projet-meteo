#include "../headers/meteo.h"

int main(int argc, char const *argv[])
{
    if ((access("assets/dataset_antartique.csv", R_OK) == 0) && (access("assets/dataset_antilles.csv", R_OK) == 0) && (access("assets/dataset_france.csv", R_OK) == 0) &&
            (access("assets/dataset_guyane.csv", R_OK) == 0) && (access("assets/dataset_oceanIndien.csv", R_OK) == 0) && (access("assets/dataset_stPierreEtMiquelon.csv", R_OK) == 0))
    {
        return(0);
    } else {
        printf("Les datasets decoupes n'existent pas ils doivent etre cree\n");
        time_t debut;          // clock contenant le temps de départ
        /* on initialise l'horloge de départ */
        debut = time(NULL);
       
        FILE* fluxSource = NULL;
        FILE* fluxFrance = NULL;
        FILE* fluxGuyane = NULL;
        FILE* fluxOceanIndien = NULL;
        FILE* fluxAntartique = NULL;
        FILE* fluxStPierreEtMiquelon = NULL;
        FILE* fluxAntilles = NULL;

        fluxSource = fopen("assets/meteo_filtered_data_v1.csv","r");
        fluxFrance = fopen("assets/dataset_france.csv","w");
        fluxGuyane = fopen("assets/dataset_guyane.csv","w");
        fluxOceanIndien = fopen("assets/dataset_oceanIndien.csv","w");
        fluxAntartique = fopen("assets/dataset_antartique.csv","w");
        fluxStPierreEtMiquelon = fopen("assets/dataset_stPierreEtMiquelon.csv","w");
        fluxAntilles = fopen("assets/dataset_antilles.csv","w");

        char ligne[BUFFER_SIZE];
        char* laLigne;
        point pointActuel;
        char* token;

        if ((fluxFrance == NULL) || (fluxGuyane == NULL) || (fluxOceanIndien == NULL) || (fluxAntartique == NULL) || 
            (fluxStPierreEtMiquelon == NULL) || (fluxAntilles == NULL) || (fluxSource == NULL))
        {
            return 1;
        } else {
        fgets(ligne,BUFFER_SIZE,fluxSource);
        while (fgets(ligne,BUFFER_SIZE,fluxSource) != NULL)
        {
            laLigne = ligne;
            while (laLigne != NULL)
            {
            token = strsep(&laLigne,";");
            pointActuel.ID = atoi(token);
            token = strsep(&laLigne,";");
            pointActuel.date = decoupageDate(token);
            token = strsep(&laLigne,";");
            pointActuel.pressionNiveauMer = atof(token);
            token = strsep(&laLigne,";");
            pointActuel.directionVent = atof(token);
            token = strsep(&laLigne,";");
            pointActuel.vitesseVent = atof(token);
            token = strsep(&laLigne,";");
            pointActuel.humidite = atof(token);
            token = strsep(&laLigne,";");
            pointActuel.pressionStation = atof(token);
            token = strsep(&laLigne,";");
            pointActuel.variationPression = atof(token);
            token = strsep(&laLigne,";");
            pointActuel.precipitations = atof(token);
            token = strsep(&laLigne,";");
            pointActuel.coordonnees = decoupageCoordonnees(token);
            token = strsep(&laLigne,";");
            pointActuel.temperature = atof(token);
            token = strsep(&laLigne,";");
            pointActuel.temperatureMin = atof(token);
            token = strsep(&laLigne,";");
            pointActuel.temperatureMax = atof(token);
            token = strsep(&laLigne,";");
            pointActuel.altitude = atof(token);
            token = strsep(&laLigne,";");
            pointActuel.codeCommune = token;
            }
            decoupagePays(fluxFrance,fluxGuyane,fluxOceanIndien,fluxAntartique,fluxStPierreEtMiquelon,fluxAntilles,pointActuel);
        }   
    }
    fclose(fluxSource);
    fclose(fluxFrance);
    fclose(fluxGuyane);
    fclose(fluxOceanIndien);
    fclose(fluxAntartique);
    fclose(fluxStPierreEtMiquelon);
    fclose(fluxAntilles);
    time_t fin = time(NULL);
    float tempsExec = difftime(fin,debut);
    printf("Temps d'exécution pour le decoupage : %f secondes\n",tempsExec);
    }
    return 0;
}
