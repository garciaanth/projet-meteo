#include "../headers/pression3.h"
#include "../headers/meteo.h"


int main(int argc, char *argv[])
{
	struct bornes valBornes;
	valBornes.dateMin.annee = atoi(argv[1]);
	valBornes.dateMin.mois = atoi(argv[2]);
	valBornes.dateMin.jour = atoi(argv[3]);
	valBornes.dateMax.annee = atoi(argv[4]);
	valBornes.dateMax.mois = atoi(argv[5]);
	valBornes.dateMax.jour = atoi(argv[6]);
	valBornes.latMin = atof(argv[7]);
	valBornes.latMax = atof(argv[8]);
	valBornes.longMin = atof(argv[9]);
	valBornes.longMax = atof(argv[10]);
  	char* input = NULL;
  	char* output = NULL;
  	input = argv[11];
  	output = argv[12];
	arbreAnnee arbreTest;
  	FILE* fluxFrance = NULL;
  	fluxFrance = fopen(input,"r");
  	arbreTest = lectureDataSet(fluxFrance,valBornes);
  	fclose(fluxFrance);
  	FILE* fluxSortie = NULL;
	char nomFichierSortieDat[1000];
  	strcpy(nomFichierSortieDat,"out/");
  	strcat(nomFichierSortieDat,output);
  	strcat(nomFichierSortieDat,".dat");
  	fluxSortie = fopen(nomFichierSortieDat,"w");
  	parcoursInfixeAnnnee(arbreTest, fluxSortie);
	ecrireGNUPLOT(output);
	fclose(fluxSortie);
	return(0);
}

arbreAnnee lectureDataSet(FILE* fluxSource,struct bornes valBornes)
{
arbreAnnee arbreAnnee = NULL;
char ligne[BUFFER_SIZE];
char* laLigne;
point pointActuel;
char* token;
if (fluxSource != NULL)
{
  while (fgets(ligne,BUFFER_SIZE,fluxSource) != NULL)
  {
      laLigne = ligne;
      while (laLigne != NULL)
      {
      token = strsep(&laLigne,";");
      pointActuel.ID = atoi(token);
      token = strsep(&laLigne,";");
      pointActuel.date = decoupageDate(token);
      token = strsep(&laLigne,";");
      pointActuel.pressionNiveauMer = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.directionVent = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.vitesseVent = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.humidite = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.pressionStation = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.variationPression = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.precipitations = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.coordonnees = decoupageCoordonnees(token);
      token = strsep(&laLigne,";");
      pointActuel.temperature = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.temperatureMin = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.temperatureMax = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.altitude = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.codeCommune = token;
      }
      arbreAnnee = insertionArbreAnnee(pointActuel, arbreAnnee);
    }
  }
  return arbreAnnee;
} 




arbreAnnee insertionArbreAnnee(point valeur, arbreAnnee annee)
{
	if (annee == NULL)
	{
		annee = creerArbreAnnee(valeur, NULL, NULL);
	}
	else if (valeur.date.annee > annee->anneeTemp)
	{
		annee->anneeFilsD = insertionArbreAnnee(valeur, annee->anneeFilsD);
	}
	else if (valeur.date.annee < annee->anneeTemp)
	{
		annee->anneeFilsG = insertionArbreAnnee(valeur, annee->anneeFilsG);
	}
	else 
	{
		annee->mois = insertionArbreMois(valeur, annee->mois);
	}
	return (annee);
}

arbreAnnee creerArbreAnnee(point valeur, arbreAnnee anneeFilsG, arbreAnnee anneeFilsD)
{
	arbreAnnee a;
	a = malloc(sizeof(noeudAnnee));
	a->anneeFilsG = anneeFilsG;
	a->anneeFilsD = anneeFilsD;
	a->anneeTemp = valeur.date.annee;
	a->mois = creerArbreMois(valeur, NULL, NULL);
	return (a);
}

arbreMois insertionArbreMois(point valeur, arbreMois mois)
{
	if (mois == NULL)
	{
		mois = creerArbreMois(valeur, NULL, NULL);
	}
	else if (valeur.date.mois > mois->moisTemp)
	{
		mois->moisFilsD = insertionArbreMois(valeur, mois->moisFilsD);
	}
	else if (valeur.date.mois < mois->moisTemp)
	{
		mois->moisFilsG = insertionArbreMois(valeur, mois->moisFilsG);
	}
	else 
	{
		mois->jours = insertionArbreJours(valeur, mois->jours);
	}
	return (mois);
}

arbreMois creerArbreMois(point valeur, arbreMois moisFilsG, arbreMois moisFilsD)
{
	arbreMois a;
	a = malloc(sizeof(noeudMois));
	a->moisFilsG = moisFilsG;
	a->moisFilsD = moisFilsD;
	a->moisTemp = valeur.date.mois;
	a->jours = creerArbreJours(valeur, NULL, NULL);
	return (a);
}

arbreJours insertionArbreJours(point valeur, arbreJours jours)
{
	if (jours == NULL)
	{
		jours = creerArbreJours(valeur, NULL, NULL);
	}
	else if (valeur.date.jour > jours->joursTemp)
	{
		jours->joursFilsD = insertionArbreJours(valeur, jours->joursFilsD);
	}
	else if (valeur.date.jour < jours->joursTemp)
	{
		jours->joursFilsG = insertionArbreJours(valeur, jours->joursFilsG);
	}
	else 
	{
		jours->heure = insertionArbreHeure(valeur, jours->heure);
	}
	return (jours);
}

arbreJours creerArbreJours(point valeur, arbreJours joursFilsG, arbreJours joursFilsD)
{
	arbreJours a;
	a = malloc(sizeof(noeudJours));
	a->joursFilsG = joursFilsG;
	a->joursFilsD = joursFilsD;
	a->joursTemp = valeur.date.jour;
	a->heure = creerArbreHeure(valeur, NULL, NULL);
	return (a);
}

arbreHeure creerArbreHeure(point valeur, arbreHeure heureFilsG, arbreHeure heureFilsD)
{
	arbreHeure a;
	a = malloc(sizeof(noeudHeure));
	a->heureFilsG = heureFilsG;
	a->heureFilsD = heureFilsD;
	a->heureTemp = valeur.date.mois;
	a->ID = creerArbreIDTemperature(valeur, NULL, NULL);
	a->nbElt = 1;
	return (a);
}

arbreHeure insertionArbreHeure(point valeur, arbreHeure heure)
{
	if (heure == NULL)
	{
		heure = creerArbreHeure(valeur, NULL, NULL);
	}
	else if (valeur.date.heure > heure->heureTemp)
	{
		heure->heureFilsD = insertionArbreHeure(valeur, heure->heureFilsD);
	}
	else if (valeur.date.heure < heure->heureTemp)
	{
		heure->heureFilsG = insertionArbreHeure(valeur, heure->heureFilsG);
	}
	else 
	{
		heure->ID = insertionArbreIDTemperature(valeur, heure->ID);
		heure->nbElt++;
	}
	return (heure);
}

arbreIDTemperature insertionArbreIDTemperature(point valeur, arbreIDTemperature ID)
{
	if (ID == NULL)
	{
		ID = creerArbreIDTemperature(valeur, NULL, NULL);
	}
	else if (valeur.ID > ID->ID)
	{
		ID->filsD = insertionArbreIDTemperature(valeur, ID->filsD);
	}
	else 
	{
		ID->filsG = insertionArbreIDTemperature(valeur, ID->filsG);
	}
	return(ID);
}

arbreIDTemperature creerArbreIDTemperature(point valeur, arbreIDTemperature filsG, arbreIDTemperature filsD)
{
	arbreIDTemperature a;
	a = malloc(sizeof(noeudIDTemperature));
	a->filsG = filsG;
	a->filsD = filsD;
	//a->heureTemp = valeur.date.mois;
	a->pression = valeur.pressionStation;
	a->ID = valeur.ID;
	return (a);
}

void parcoursInfixeIDTemperature(arbreIDTemperature a, FILE* fluxSortie, int annee, int mois, int jours, int heure)
{
	if (a == NULL)
	{

	}
	else
	{
		parcoursInfixeIDTemperature(a->filsG, fluxSortie, annee, mois, jours, heure);
		fprintf(fluxSortie,"%f;%d/%d/%d/%d;%d\n",a->pression, heure, jours, mois, annee, a->ID);
		parcoursInfixeIDTemperature(a->filsD, fluxSortie, annee, mois, jours, heure);
	}
}

void parcoursInfixeHeure(arbreHeure a, FILE* fluxSortie, int annee, int mois, int jours)
{
	if (a == NULL)
	{

	}
	else
	{
		parcoursInfixeHeure(a->heureFilsG, fluxSortie, annee, mois, jours);
		parcoursInfixeIDTemperature(a->ID, fluxSortie, annee, mois, jours, a->heureTemp);
		parcoursInfixeHeure(a->heureFilsD, fluxSortie, annee, mois, jours);
	}
}

void parcoursInfixeJours(arbreJours a, FILE* fluxSortie, int annee, int mois)
{
	if (a == NULL)
	{

	}
	else
	{
		parcoursInfixeJours(a->joursFilsG, fluxSortie, annee, mois);
		parcoursInfixeHeure(a->heure, fluxSortie, annee, mois, a->joursTemp);
		parcoursInfixeJours(a->joursFilsD, fluxSortie, annee, mois);
	}
}

void parcoursInfixeMois(arbreMois a, FILE* fluxSortie, int annee)
{
	if (a == NULL)
	{

	}
	else
	{
		parcoursInfixeMois(a->moisFilsG, fluxSortie, annee);
		parcoursInfixeJours(a->jours, fluxSortie, annee, a->moisTemp);
		parcoursInfixeMois(a->moisFilsD, fluxSortie, annee);
	}
}

void parcoursInfixeAnnnee(arbreAnnee a, FILE* fluxSortie)
{
	if (a == NULL)
	{

	}
	else
	{
		parcoursInfixeAnnnee(a->anneeFilsG, fluxSortie);
		parcoursInfixeMois(a->mois, fluxSortie, a->anneeTemp);
		parcoursInfixeAnnnee(a->anneeFilsD, fluxSortie);
	}
}


void ecrireGNUPLOT(char* output){
FILE *fichierGP = fopen("plot.gp", "w");
fprintf(fichierGP,"set datafile separator ';'\n");  
fprintf(fichierGP,"set title textcolor 'red'\n");
fprintf(fichierGP,"set title 'graphique humidité'\n");
fprintf(fichierGP,"set xlabel 'longitude'\n");  
fprintf(fichierGP,"set ylabel 'latitude'\n");
fprintf(fichierGP,"set pm3d\n");
fprintf(fichierGP,"set dgrid3d 100,100,3\n");  
fprintf(fichierGP,"set autoscale x\n");
fprintf(fichierGP,"set autoscale y\n");
fprintf(fichierGP,"set terminal png\n");
fprintf(fichierGP,"set output 'png/%s.png'\n",output);
fprintf(fichierGP,"splot 'out/%s.dat' u 1:2:3 w pm3d\n",output);
fclose(fichierGP);
}