#include "../headers/humidite.h"
#include "../headers/meteo.h"

int main(int argc, char *argv[])
{
  arbreID arbreTest;
  FILE* fluxFrance = NULL;
  struct bornes valBornes;
  valBornes.dateMin.annee = atoi(argv[1]);
  valBornes.dateMin.mois = atoi(argv[2]);
  valBornes.dateMin.jour = atoi(argv[3]);
  valBornes.dateMax.annee = atoi(argv[4]);
  valBornes.dateMax.mois = atoi(argv[5]);
  valBornes.dateMax.jour = atoi(argv[6]);
  valBornes.latMin = atof(argv[7]);
  valBornes.latMax = atof(argv[8]);
  valBornes.longMin = atof(argv[9]);
  valBornes.longMax = atof(argv[10]);
  char* input = NULL;
  char* output = NULL;
  input = argv[11];
  output = argv[12];
  fluxFrance = fopen(input,"r");
  arbreTest = lectureDataSet(fluxFrance,valBornes);
  fclose(fluxFrance);
  FILE* fluxSortie = NULL;
  char nomFichierSortieDat[1000];
  strcpy(nomFichierSortieDat,"out/");
  strcat(nomFichierSortieDat,output);
  strcat(nomFichierSortieDat,".dat");
  fluxSortie = fopen(nomFichierSortieDat,"w");
  ecrireInfixeID(arbreTest,fluxSortie);
  fclose(fluxSortie);
  ecrireGNUPLOT(output);
  return 0;
}

arbre creerABR(point valeur,arbre filsG,arbre filsD){
  arbre a;
  a = malloc(sizeof(noeud));
  a->valeur = valeur;
  a->filsG = filsG;
  a->filsD = filsD;
  return(a);
}

arbreID creerArbreID(int valeur,arbreID filsG,arbreID filsD){
  arbreID a;
  a = malloc(sizeof(noeudID));
  a->valeur.ID = valeur;
  a->filsG = filsG;
  a->filsD = filsD;
  return(a);
}

arbre insertionArbre(point valeur,arbre a){
  if (a == NULL || valeur.humidite > a->valeur.humidite)
  {
    a = creerABR(valeur,NULL,NULL);
  }
  return(a);
}

arbreID insertionArbreID(point valeur,arbreID a){
  if (a == NULL)
  {
    a = creerArbreID(valeur.ID,NULL,NULL);
  } else if (valeur.ID > a->valeur.ID)
    {
      a->filsD = (insertionArbreID(valeur,a->filsD));
    } else if (valeur.ID < a->valeur.ID) 
    {
      a->filsG = (insertionArbreID(valeur,a->filsG));
    } else if (valeur.ID == a->valeur.ID)
    {
      a->arbrePoint = insertionArbre(valeur,a->arbrePoint);
    }
  return(a);
}

int minArbre(arbre a){
  if (a->filsG == NULL)
  {
    return(a->valeur.ID);
  } else {
    return(minArbre(a->filsG));
  }
}

int maxArbre(arbre a){
  if (a->filsD == NULL)
  {
    return(a->valeur.ID);
  } else {
    return(maxArbre(a->filsD));
  }
}

arbre insertionAVL(point valeur, arbre a){
  a = insertionArbre(valeur, a);
  a = equilibrage(a);
  return a;
}

arbre rotationGauche(arbre b){
  arbre a;
  if (b->filsD == NULL)
  {
    return b;
  }
  else
  {
    a = b->filsD;
    b = creerABR(b->valeur, b->filsG, a->filsG);
    a = creerABR(a->valeur, b, a->filsD);
    return a;
  }
}

arbre rotationDroite(arbre a){
  arbre b;
  if (a->filsG == NULL)
  {
    return a;
  }
  else 
  {
    b = a->filsG;
    a = creerABR(a->valeur, b->filsD, a->filsD);
    b = creerABR(b->valeur, b->filsG, a);
    return b;
  }
}

arbre rotationGaucheDroite(arbre a){
  //printf("On lance la rotation Gauche\n");
  if (a->filsD == NULL)
  {
    return a;
  }
  else 
  {
    a->filsG = rotationGauche(a->filsG);
    return rotationDroite(a);
  }
}

arbre rotationDroiteGauche(arbre a){
  if ( a->filsD == NULL)
  {
    return a;
  }
  else 
  {
    a->filsD = rotationDroite(a->filsD);
    return rotationGauche(a);
  }
}

arbre equilibrage(arbre a)
{
  int e;
  e = ecart(a);
  if (e == 2)
  {
      if (ecart(a->filsG)==1) 
      {
          a = rotationDroite(a);
      }
      else
      {
          a = rotationGaucheDroite(a);
      }
  } 
  else if (e== -2) 
  {
    if (ecart(a->filsD)==-1)
    {
        a = rotationGauche(a);
    }
    else 
    {
        a = rotationDroiteGauche(a);
    }
  }
  return a;
}


int hauteur(arbre a){
  if ((a == NULL) || ((a->filsD == NULL) && (a->filsG == NULL))) {
    return 0;
  }else {
    return 1+(MAX(hauteur(a->filsG),hauteur(a->filsD)));
  }
}

int ecart(arbre a){
    int res;
    if (a == NULL) {
        res = 0;
    }else{
        res = (hauteur(a->filsD) - hauteur(a->filsG));
    }
    return res;
}


void parcoursLargeur(arbre a){
  if (a == NULL)
  {
    printf("\n");
  } else {
    printf("%d\n",a->valeur.ID);
    parcoursLargeur(a->filsG);
    parcoursLargeur(a->filsD);
  }
}

void afficherInfixe(arbre a){
  if (a == NULL) {
    printf("\n");
  } else {
    afficherInfixe(a->filsG);
    printf("%d |",a->valeur.ID);
    afficherInfixe(a->filsD);
  }
}

void afficherInfixeID(arbreID a){
  if (a == NULL) {
    printf("\n");
  } else {
    afficherInfixeID(a->filsG);
    printf("%d | ",a->valeur.ID);
    printf("%f",a->arbrePoint->valeur.humidite);
    afficherInfixeID(a->filsD);

  }
}

void ecrireInfixeID(arbreID a,FILE* fluxSortie){
    if (a == NULL) {
  } else {
    ecrireInfixeID(a->filsG,fluxSortie);
    fprintf(fluxSortie,"%f;",a->arbrePoint->valeur.coordonnees.longitude);
    fprintf(fluxSortie,"%f;",a->arbrePoint->valeur.coordonnees.latitude);
    fprintf(fluxSortie,"%f;\n",a->arbrePoint->valeur.humidite);
    ecrireInfixeID(a->filsD,fluxSortie);
  }
}


arbreID lectureDataSet(FILE* fluxSource,struct bornes valBornes)
{
arbreID arbreID = NULL;
char ligne[BUFFER_SIZE];
char* laLigne;
point pointActuel;
char* token;
if (fluxSource != NULL)
{
  while (fgets(ligne,BUFFER_SIZE,fluxSource) != NULL)
  {
      laLigne = ligne;
      while (laLigne != NULL)
      {
      token = strsep(&laLigne,";");
      pointActuel.ID = atoi(token);
      token = strsep(&laLigne,";");
      pointActuel.date = decoupageDate(token);
      token = strsep(&laLigne,";");
      pointActuel.pressionNiveauMer = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.directionVent = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.vitesseVent = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.humidite = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.pressionStation = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.variationPression = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.precipitations = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.coordonnees = decoupageCoordonnees(token);
      token = strsep(&laLigne,";");
      pointActuel.temperature = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.temperatureMin = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.temperatureMax = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.altitude = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.codeCommune = token;
      }
      if (dansLesBornes(valBornes,pointActuel) == 0)
      {
        arbreID = insertionArbreID(pointActuel, arbreID);
      }
    }
  }
  return arbreID;
} 

int comptageNoeud(arbre a)
{
  if (a == NULL)
  {
    return 0;
  }
  return 1 + comptageNoeud(a->filsG) + comptageNoeud(a->filsD);
}





int dansLesBornes(struct bornes valBornes,point valeur){
  int res = 1;
  if ((valeur.date.annee <= valBornes.dateMax.annee) && (valeur.date.annee >= valBornes.dateMin.annee))
  {
    if (valeur.date.annee == valBornes.dateMax.annee && valeur.date.mois < valBornes.dateMin.mois)
    {
      return(res);
    }
    if (valeur.date.annee == valBornes.dateMax.annee && valeur.date.mois > valBornes.dateMax.mois)
    {
      return(res);
    }
    if (valeur.date.mois >= valBornes.dateMin.mois && valeur.date.mois <= valBornes.dateMax.mois)
    {
      if (valeur.date.mois == valBornes.dateMin.mois && valeur.date.jour < valBornes.dateMin.jour)
      {
        return(res);
      }
      if (valeur.date.mois == valBornes.dateMax.mois && valeur.date.jour > valBornes.dateMax.jour)
      {
        return(res);
      }
      if (valeur.date.jour >= valBornes.dateMin.jour && valeur.date.jour <= valBornes.dateMax.jour)
      {
        if ((valeur.coordonnees.latitude >= valBornes.latMin) && (valeur.coordonnees.latitude <= valBornes.latMax) &&
            (valeur.coordonnees.longitude >= valBornes.longMin) && (valeur.coordonnees.longitude <= valBornes.longMax))
        {
          res = 0;
        }
      }
    }
  }
  return(res);
}

void ecrireGNUPLOT(char* output){
  FILE *fichierGP = fopen("plot.gp", "w");
  fprintf(fichierGP,"set datafile separator ';'\n");  
  fprintf(fichierGP,"set title textcolor 'red'\n");
  fprintf(fichierGP,"set title 'graphique humidité'\n");
  fprintf(fichierGP,"set xlabel 'longitude'\n");  
  fprintf(fichierGP,"set ylabel 'latitude'\n");
  fprintf(fichierGP,"set pm3d\n");
  fprintf(fichierGP,"set dgrid3d 100,100,3\n");  
  fprintf(fichierGP,"set autoscale x\n");
  fprintf(fichierGP,"set autoscale y\n");
  fprintf(fichierGP,"set terminal png\n");
  fprintf(fichierGP,"set output 'png/%s.png'\n",output);
  fprintf(fichierGP,"splot 'out/%s.dat' u 1:2:3 w pm3d\n",output);
  fclose(fichierGP);
}
