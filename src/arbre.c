#include "../headers/arbre.h"
#include "../headers/meteo.h"
/*
arbre creerArbrePoint(point valeur,arbre filsG,arbre filsD){
  arbre a;
  a = malloc(sizeof(noeud));
  a->valeur = valeur;
  a->filsG = filsG;
  a->filsD = filsD;
  return(a);
}

arbreID creerArbreID(int valeur,arbreID filsG,arbreID filsD){
  arbreID a;
  a = malloc(sizeof(noeudID));
  a->ID = valeur;
  a->filsG = filsG;
  a->filsD = filsD;
  return(a);
}

void afficherInfixe(arbre a){
  if (a == NULL) {
    printf("\n");
  } else {
    afficherInfixe(a->filsG);
    printf("%d |",a->valeur.ID);
    afficherInfixe(a->filsD);
  }
}

void afficherInfixeID(arbreID a){
  if (a == NULL) {
    printf("\n");
  } else {
    afficherInfixeID(a->filsG);
    printf("%d |",a->ID);
    afficherInfixeID(a->filsD);
  }
}

int comptageNoeud(arbre a)
{
  if (a == NULL)
  {
    return 0;
  }
  return 1 + comptageNoeud(a->filsG) + comptageNoeud(a->filsD);
}

void ecrireInfixeID(arbreID a,FILE* fluxSortie){
    if (a == NULL) {
  } else {
    ecrireInfixeID(a->filsG,fluxSortie);
    fprintf(fluxSortie,"%f;",a->arbrePoint->valeur.coordonnees.longitude);
    fprintf(fluxSortie,"%f;",a->arbrePoint->valeur.coordonnees.latitude);
    fprintf(fluxSortie,"%f;\n",a->arbrePoint->valeur.humidite);
    ecrireInfixeID(a->filsD,fluxSortie);
  }
}



arbre rechercheArbre(point valeur,arbre a){
  if (a == NULL)
  {
    printf("Votre valeur n'existe pas\n");
    return(NULL);
  } else {
    if (a->valeur == valeur)
    {
      return(a); 
    } else if (valeur > a->valeur)
    {
      return(rechercheArbre(valeur,a->filsD));
    } else {
      return(rechercheArbre(valeur,a->filsG));
    }
  }
}

void afficherInfixe(arbre a){
  if (a == NULL) {
    printf("\n");
  } else {
    afficherInfixe(a->filsG);
    afficherStruct(a->filsG)
    afficherInfixe(a->filsD);
  }
}

arbre insertionArbre(point valeur,arbre a){
  if (a == NULL)
  {
    a = creerABR(valeur,NULL,NULL);
  } else {
    if (valeur > a->valeur)
    {
      a->filsD = (insertionArbre(valeur,a->filsD));
    } else {
      a->filsG = (insertionArbre(valeur,a->filsG));
    }
  }
  return(a);
}

int minArbre(arbre a){
  if (a->filsG == NULL)
  {
    return(a->valeur);
  } else {
    return(minArbre(a->filsG));
  }
}

int maxArbre(arbre a){
  if (a->filsD == NULL)
  {
    return(a->valeur);
  } else {
    return(maxArbre(a->filsD));
  }
}

arbre supprimerArbre(point valeur, arbre a){
  int max = 0;
  if (a == NULL)
  {
    return(a);
  } else if (valeur > a->valeur)
  {
    return(creerABR(a->valeur,a->filsG,supprimerArbre(valeur,a->filsD)));
  } else if (valeur < a->valeur)
  {
    return(creerABR(a->valeur,supprimerArbre(valeur,a->filsG),a->filsD));
  } else {
    max = maxArbre(a->filsG);
    return(creerABR(max,supprimerArbre(max,a->filsG),a->filsD));
  }
}

void parcoursLargeur(arbre a){
  if (a == NULL)
  {
    printf("\n");
  } else {
    printf("%d\n",a->valeur);
    parcoursLargeur(a->filsG);
    parcoursLargeur(a->filsD);
  }
  
}

arbre insertionAVL(point valeur, arbre a){
	a = insertionArbre(valeur, a);
	a = equilibrage(a);
	return a;
}

arbre suppressionAVL(point valeur, arbre a){
	a = supprimerArbre(valeur, a);
	a = equilibrage(a);
	return a;
}

arbre rotationGauche(arbre b){
	arbre a;
    a = b->filsD;
    b = creerABR(b->valeur, b->filsG, a->filsG);
    a = creerABR(a->valeur, b, a->filsD);
    return a;
}

arbre rotationDroite(arbre a){
	arbre b;
    b = a->filsG;
    a = creerABR(a->valeur, b->filsD, a->filsD);
    b = creerABR(b->valeur, b->filsG, a);
    return b;
}

arbre rotationGaucheDroite(arbre a){
	a->filsG = rotationGauche(a->filsG);
	return rotationDroite(a);
}

arbre rotationDroiteGauche(arbre a){
	a->filsD = rotationDroite(a->filsD);
	return rotationGauche(a);
}

arbre equilibrage(arbre a){
	int e;
    e = ecart(a);
    if (e == 2){
        if (ecart(a->filsG)==1) {
            a = rotationDroite(a);
        }else{
            a = rotationGaucheDroite(a);
        }
    } else if (e== -2) {
        if (ecart(a->filsD)==-1){
            a = rotationGauche(a);
      	}else {
            a = rotationDroiteGauche(a);
        }
    }
	return a;
}


int hauteur(arbre a){
  if ((a == NULL) || (a->filsD == NULL && a->filsG == NULL)) {
    return 0;
  }else {
    return 1+(MAX(hauteur(a->filsG),hauteur(a->filsD)));
  }
}

int ecart(arbre a){
    int res;
    if (a == NULL) {
        res = 0;
    }else{
        res = (hauteur(a->filsD) - hauteur(a->filsG));
    }
    return res;
}*/
