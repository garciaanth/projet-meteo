#include "../headers/vent.h"
#include "../headers/meteo.h"

int main(int argc, char *argv[])
{
  arbreID arbreTest;
  struct bornes valBornes;
  valBornes.dateMin.annee = atoi(argv[1]);
  valBornes.dateMin.mois = atoi(argv[2]);
  valBornes.dateMin.jour = atoi(argv[3]);
  valBornes.dateMax.annee = atoi(argv[4]);
  valBornes.dateMax.mois = atoi(argv[5]);
  valBornes.dateMax.jour = atoi(argv[6]);
  valBornes.latMin = atof(argv[7]);
  valBornes.latMax = atof(argv[8]);
  valBornes.longMin = atof(argv[9]);
  valBornes.longMax = atof(argv[10]);
  char* input = NULL;
  char* output = NULL;
  input = argv[11];
  output = argv[12];
  FILE* fluxFrance = NULL;
  fluxFrance = fopen(input,"r");
  arbreTest = lectureDataSet(fluxFrance,valBornes);
  fclose(fluxFrance);
  FILE* fluxSortie = NULL;
  char nomFichierSortieDat[1000];
  strcpy(nomFichierSortieDat,"out/");
  strcat(nomFichierSortieDat,output);
  strcat(nomFichierSortieDat,".dat");
  fluxSortie = fopen(nomFichierSortieDat,"w");
  ecrireInfixeID(arbreTest,fluxSortie);
  fclose(fluxSortie);
  ecrireGNUPLOT(output);
  return 0;
}

float degreesToRad(float valeurEnDegres){
    float valeurEnRadians;
    valeurEnRadians = (valeurEnDegres * (M_PI / 180));
    return(valeurEnRadians);
}

arbreID lectureDataSet(FILE* fluxSource,struct bornes valBornes)
{
arbreID arbreID = NULL;
char ligne[BUFFER_SIZE];
char* laLigne;
point pointActuel;
char* token;
if (fluxSource != NULL)
{
  while (fgets(ligne,BUFFER_SIZE,fluxSource) != NULL)
  {
      laLigne = ligne;
      while (laLigne != NULL)
      {
      token = strsep(&laLigne,";");
      pointActuel.ID = atoi(token);
      token = strsep(&laLigne,";");
      pointActuel.date = decoupageDate(token);
      token = strsep(&laLigne,";");
      pointActuel.pressionNiveauMer = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.directionVent = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.vitesseVent = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.humidite = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.pressionStation = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.variationPression = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.precipitations = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.coordonnees = decoupageCoordonnees(token);
      token = strsep(&laLigne,";");
      pointActuel.temperature = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.temperatureMin = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.temperatureMax = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.altitude = atof(token);
      token = strsep(&laLigne,";");
      pointActuel.codeCommune = token;
      }
      if (dansLesBornes(valBornes,pointActuel) == 0)
      {
        arbreID = insertionArbreID(pointActuel, arbreID);
      }
    }
  }
  return arbreID;
} 

arbreID insertionArbreID(point valeur,arbreID a){
  if ((a == NULL))
  {
    a = creerArbreID(valeur.ID,valeur.vitesseVent,valeur.directionVent,valeur.coordonnees.longitude,valeur.coordonnees.latitude,NULL,NULL);
  } else if ((valeur.ID > a->valeur.ID))
    {
      a->filsD = (insertionArbreID(valeur,a->filsD));
    } else if ((valeur.ID < a->valeur.ID)) 
    {
      a->filsG = (insertionArbreID(valeur,a->filsG));
    } else if ((valeur.ID == a->valeur.ID))
    {
      if (valeur.directionVent > 0 && valeur.directionVent <= 90)
      {
        a->valeurVent.vitesseX = a->valeurVent.vitesseX + (valeur.vitesseVent*cos(degreesToRad(valeur.directionVent)));
        a->valeurVent.vitesseY = a->valeurVent.vitesseY + (valeur.vitesseVent*sin(degreesToRad(valeur.directionVent)));
        a->valeurVent.nbElt =  a->valeurVent.nbElt + 1;
      } else if (valeur.directionVent > 90 && valeur.directionVent <= 180)
      {
        a->valeurVent.vitesseX = a->valeurVent.vitesseX + ((-1)*valeur.vitesseVent*sin(degreesToRad(valeur.directionVent)));
        a->valeurVent.vitesseY = a->valeurVent.vitesseY + (valeur.vitesseVent*cos(degreesToRad(valeur.directionVent)));
        a->valeurVent.nbElt =  a->valeurVent.nbElt + 1;
      } else if (valeur.directionVent > 180 && valeur.directionVent <= 270)
      {
        a->valeurVent.vitesseX = a->valeurVent.vitesseX + ((-1)*valeur.vitesseVent*cos(degreesToRad(valeur.directionVent)));
        a->valeurVent.vitesseY = a->valeurVent.vitesseY + ((-1)*valeur.vitesseVent*sin(degreesToRad(valeur.directionVent)));
        a->valeurVent.nbElt =  a->valeurVent.nbElt + 1;
      } else {
        a->valeurVent.vitesseX = a->valeurVent.vitesseX + (valeur.vitesseVent*sin(degreesToRad(valeur.directionVent)));
        a->valeurVent.vitesseY = a->valeurVent.vitesseY + ((-1)*valeur.vitesseVent*cos(degreesToRad(valeur.directionVent)));
        a->valeurVent.nbElt =  a->valeurVent.nbElt + 1;
      }
    }
  return(a);
}

arbreID creerArbreID(int ID,float vitesse, float orientation,float longitude, float latitude,arbreID filsG,arbreID filsD){
  arbreID a;
  a = malloc(sizeof(noeudID));
  a->valeur.ID = ID;
  a->valeurVent.valCoordonnees.longitude = longitude;
  a->valeurVent.valCoordonnees.latitude = latitude;
  if ((vitesse != 0) && (orientation != 0))
  {
    if (orientation > 0 && orientation <= 90)
    {
      a->valeurVent.vitesseX = (vitesse*cos(degreesToRad(orientation)));
      a->valeurVent.vitesseY = (vitesse*sin(degreesToRad(orientation)));
      a->valeurVent.nbElt = 1;
    } else if (orientation > 90 && orientation <= 180)
    {
      a->valeurVent.vitesseX = ((-1)*vitesse*sin(degreesToRad(orientation)));
      a->valeurVent.vitesseY = (vitesse*cos(degreesToRad(orientation)));
      a->valeurVent.nbElt = 1;
    } else if (orientation > 180 && orientation <= 270)
    {
      a->valeurVent.vitesseX = ((-1)*vitesse*cos(degreesToRad(orientation)));
      a->valeurVent.vitesseY = ((-1)*vitesse*sin(degreesToRad(orientation)));
      a->valeurVent.nbElt = 1;
    } else {
      a->valeurVent.vitesseX = (vitesse*sin(degreesToRad(orientation)));
      a->valeurVent.vitesseY = ((-1)*vitesse*cos(degreesToRad(orientation)));
      a->valeurVent.nbElt = 1;
    }
  } else {
  a->valeurVent.vitesseMoyX = 0;
  a->valeurVent.vitesseMoyY = 0;
  a->valeurVent.vitesseX = 0;
  a->valeurVent.vitesseY = 0;
  a->valeurVent.nbElt = 0;
  a->filsG = filsG;
  a->filsD = filsD;
  }
    return(a);
}

void afficherInfixeID(arbreID a){
  if (a == NULL) {
    printf("\n");
  } else {
    afficherInfixeID(a->filsG);
    printf("%d | ",a->valeur.ID);
    a->valeurVent.vitesseMoyX = (a->valeurVent.vitesseX/a->valeurVent.nbElt);
    a->valeurVent.vitesseMoyY = (a->valeurVent.vitesseY/a->valeurVent.nbElt);
    printf("%f | ",(a->valeurVent.vitesseMoyX));
    printf("%f | ",(a->valeurVent.vitesseMoyY));
    afficherInfixeID(a->filsD);

  }
}

void ecrireInfixeID(arbreID a,FILE* fluxSortie){
    if (a == NULL) {
  } else {
    ecrireInfixeID(a->filsG,fluxSortie);
    fprintf(fluxSortie,"%f;",a->valeurVent.valCoordonnees.longitude);
    fprintf(fluxSortie,"%f;",a->valeurVent.valCoordonnees.latitude);
    fprintf(fluxSortie,"%f;",a->valeurVent.vitesseMoyX);
    fprintf(fluxSortie,"%f;\n",a->valeurVent.vitesseMoyY);
    free(a);
    ecrireInfixeID(a->filsD,fluxSortie);
}
}



int dansLesBornes(struct bornes valBornes,point valeur){
  int res = 1;
  if ((valeur.date.annee <= valBornes.dateMax.annee) && (valeur.date.annee >= valBornes.dateMin.annee))
  {
    if (valeur.date.annee == valBornes.dateMax.annee && valeur.date.mois < valBornes.dateMin.mois)
    {
      return(res);
    }
    if (valeur.date.annee == valBornes.dateMax.annee && valeur.date.mois > valBornes.dateMax.mois)
    {
      return(res);
    }
    if (valeur.date.mois >= valBornes.dateMin.mois && valeur.date.mois <= valBornes.dateMax.mois)
    {
      if (valeur.date.mois == valBornes.dateMin.mois && valeur.date.jour < valBornes.dateMin.jour)
      {
        return(res);
      }
      if (valeur.date.mois == valBornes.dateMax.mois && valeur.date.jour > valBornes.dateMax.jour)
      {
        return(res);
      }
      if (valeur.date.jour >= valBornes.dateMin.jour && valeur.date.jour <= valBornes.dateMax.jour)
      {
        if ((valeur.coordonnees.latitude >= valBornes.latMin) && (valeur.coordonnees.latitude <= valBornes.latMax) &&
            (valeur.coordonnees.longitude >= valBornes.longMin) && (valeur.coordonnees.longitude <= valBornes.longMax))
        {
          res = 0;
        }
      }
    }
  }
  return(res);
}

void ecrireGNUPLOT(char* output){
  FILE *fichierGP = fopen("plot.gp", "w");
  fprintf(fichierGP,"set datafile separator ';'\n");  
  fprintf(fichierGP,"set title 'vent'\n");
  fprintf(fichierGP,"set xlabel 'longitute'\n");
  fprintf(fichierGP,"set ylabel 'latitude'\n");  
  fprintf(fichierGP,"set autoscale x\n");
  fprintf(fichierGP,"set autoscale y\n");
  fprintf(fichierGP,"set terminal png\n");  
  fprintf(fichierGP,"set output 'png/%s.png'\n",output);
  fprintf(fichierGP,"plot 'out/%s.dat' using 1:2:3:4 w vectors lt 10\n",output);
  fclose(fichierGP);
  system("gnuplot plot.gp");
}
