#include "../headers/meteo.h"

void lectureFichier(char* fichierSource){
FILE* fluxSource = NULL;
char ligne[BUFFER_SIZE];
char* laLigne;
point pointActuel;
char* token;
fluxSource = fopen(fichierSource,"r");
if (fluxSource != NULL)
{
    fgets(ligne,BUFFER_SIZE,fluxSource);
    while (fgets(ligne,BUFFER_SIZE,fluxSource) != NULL)
    {
        laLigne = ligne;
        while (laLigne != NULL)
        {
        token = strsep(&laLigne,";");
        pointActuel.ID = atoi(token);
        token = strsep(&laLigne,";");
        pointActuel.date = decoupageDate(token);
        token = strsep(&laLigne,";");
        pointActuel.pressionNiveauMer = atof(token);
        token = strsep(&laLigne,";");
        pointActuel.directionVent = atof(token);
        token = strsep(&laLigne,";");
        pointActuel.vitesseVent = atof(token);
        token = strsep(&laLigne,";");
        pointActuel.humidite = atof(token);
        token = strsep(&laLigne,";");
        pointActuel.pressionStation = atof(token);
        token = strsep(&laLigne,";");
        pointActuel.variationPression = atof(token);
        token = strsep(&laLigne,";");
        pointActuel.precipitations = atof(token);
        token = strsep(&laLigne,";");
        pointActuel.coordonnees = decoupageCoordonnees(token);
        token = strsep(&laLigne,";");
        pointActuel.temperature = atof(token);
        token = strsep(&laLigne,";");
        pointActuel.temperatureMin = atof(token);
        token = strsep(&laLigne,";");
        pointActuel.temperatureMax = atof(token);
        token = strsep(&laLigne,";");
        pointActuel.altitude = atof(token);
        token = strsep(&laLigne,";");
        pointActuel.codeCommune = token;
        }
    }
}
free(laLigne);
afficherStruct(pointActuel);
fclose(fluxSource);
}

void afficherStruct(point input){
    printf("ID : %d\n",input.ID);
    printf("Date : %d/%d/%d\n",input.date.annee,input.date.mois,input.date.jour);
    printf("Pression Niveau Mer : %f\n",input.pressionNiveauMer); 
    printf("Direction Vent : %f\n",input.directionVent);
    printf("Vitesse Vent : %f\n",input.vitesseVent);
    printf("Humidite : %f\n",input.humidite);
    printf("Pression Station : %f\n",input.pressionStation);
    printf("Variation Pression : %f\n",input.variationPression);
    printf("Precipitation : %f\n",input.precipitations);
    printf("Coordoonees : %f,%f\n",input.coordonnees.latitude,input.coordonnees.latitude);
    printf("Temperature : %f\n",input.temperature);
    printf("Temperature Minimale : %f\n",input.temperatureMin);
    printf("Temperature Maximale : %f\n",input.temperatureMax);
    printf("Altitude : %f\n",input.altitude);
    printf("Code Communue : %s\n",input.codeCommune);
}

void decoupagePays(FILE* fluxFrance,FILE* fluxGuyane,FILE* fluxOceanIndien,FILE* fluxAntartique,FILE* fluxStPierreEtMiquelon,FILE* fluxAntilles,point pointAEcrire){
    if (pointAEcrire.ID >= 7005 && pointAEcrire.ID <= 7790)
    {
        ecritureFichier(fluxFrance,pointAEcrire);
    } else if (pointAEcrire.ID >= 81401 && pointAEcrire.ID <= 81415)
    {
        ecritureFichier(fluxGuyane,pointAEcrire);
    } else if (pointAEcrire.ID >= 61968 && pointAEcrire.ID <= 61996)
    {
        ecritureFichier(fluxOceanIndien,pointAEcrire);
    } else if (pointAEcrire.ID == 61988 || pointAEcrire.ID == 89642)
    {
        ecritureFichier(fluxAntartique,pointAEcrire);
    } else if (pointAEcrire.ID == 71805)
    {
        ecritureFichier(fluxStPierreEtMiquelon,pointAEcrire);
    } else if (pointAEcrire.ID >= 78890 && pointAEcrire.ID <= 78925)
    {
        ecritureFichier(fluxAntilles,pointAEcrire);
    }
}


void ecritureFichier(FILE* fluxSortie,point pointAEcrire){
    fprintf(fluxSortie,"%d;",pointAEcrire.ID);
    fprintf(fluxSortie,"%d-",pointAEcrire.date.annee);
    fprintf(fluxSortie,"%d-",pointAEcrire.date.mois);
    fprintf(fluxSortie,"%dT",pointAEcrire.date.jour);
    fprintf(fluxSortie,"%d:;",pointAEcrire.date.heure);
    fprintf(fluxSortie,"%f;",pointAEcrire.pressionNiveauMer);
    fprintf(fluxSortie,"%f;",pointAEcrire.directionVent);
    fprintf(fluxSortie,"%f;",pointAEcrire.vitesseVent);
    fprintf(fluxSortie,"%f;",pointAEcrire.humidite);
    fprintf(fluxSortie,"%f;",pointAEcrire.pressionStation);
    fprintf(fluxSortie,"%f;",pointAEcrire.variationPression);
    fprintf(fluxSortie,"%f;",pointAEcrire.precipitations);
    fprintf(fluxSortie,"%f,",pointAEcrire.coordonnees.latitude);
    fprintf(fluxSortie,"%f;",pointAEcrire.coordonnees.longitude);
    fprintf(fluxSortie,"%f;",pointAEcrire.temperature);
    fprintf(fluxSortie,"%f;",pointAEcrire.temperatureMin);
    fprintf(fluxSortie,"%f;",pointAEcrire.temperatureMax);
    fprintf(fluxSortie,"%f;",pointAEcrire.altitude);
    fprintf(fluxSortie,"%s",pointAEcrire.codeCommune);
}

struct date decoupageDate(char* tokenEntree){
  struct date sortie;
  char* strToken;
  char* tmp = strdup(tokenEntree);
  strToken = strsep(&tmp,"-");
  sortie.annee = atoi(strToken);
  strToken = strsep(&tmp,"-");
  sortie.mois = atoi(strToken);
  strToken = strsep(&tmp,"T");
  sortie.jour = atoi(strToken);
  strToken = strsep(&tmp,":");
  sortie.heure = atoi(strToken);
  return(sortie);
}

struct coordonnees decoupageCoordonnees(char* tokenEntree){
  struct coordonnees sortie;
  char* tmp = strdup(tokenEntree);
  char* strToken;
  strToken = strsep(&tmp,",");
  sortie.latitude = atof(strToken);
  strToken = strsep(&tmp,";");
  sortie.longitude = atof(strToken);
  return(sortie);

}

void help_c()
{
    printf("Vous avez demandé le fichier aide\n\n");
    printf("-t <mode>  : (t)emperatures. \n");
    printf("-p <mode> : (p)ressions atmosphériques.\n\n");
    printf("Les modes :\n\n1 : produit en sortie les températures (ou pressions)\n     minimales, maximales et moyennes par station dans l'ordre\n");
    printf("2 : produit en sortie les températures (ou  pressions)\n       moyennes par date/heure, triées dans l'ordre \n     chronologique. La moyenne se fait sur toutes les stations. \n");
    printf("3 : produit en sortie les températures (ou  pressions)  par \n     date/heure par station. Elles seront triées d'abord par ordre\n     chronologique, puis par ordre croissant de l'identifiant de la station\n\n");
    printf("-w : vent \n");
    printf("-h : (h)auteur. \n");
    printf("-m : humidité (m)oisture. \n \n");
    printf("Attention les arguments au dessus sont exclusif -> On ne peut pas en utiliser 2 a la fois\n \n");
    printf("-i <nom_fichier> : (f)ichier d'entrée.\n");
    printf("Attention cet argument est obligatoire\n \n");
    printf("o <nom_fichier> : (f)ichier de sortie. \n");
    printf("Attention cet argument est obligatoire\n \n");
    printf("-g <min> <max> : lon(g)itude \n");
    printf("-a <min> <max> : l(a)titude. \n");
    printf("-d <min> <max> : (d)ates. \n \n");
    printf("Les options -g, -a et -d sont optionnelles. \n");
    printf("Les options peuvent être rentrées dans n'importe quel ordre.\n");
}