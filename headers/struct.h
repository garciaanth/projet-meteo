#ifndef __STRUCT_H__
#define __STRUCT_H__

#include <stdlib.h> //REQUIS
#include <stdio.h>  //REQUIS
#include <getopt.h> //pour les arguments
#include <string.h> //pour strsep
#include <time.h>   //pour le temps d'execution
#include <unistd.h> //pour la fonction acces
#include <math.h>   //pour les cos et sin

#define MAX(a,b) (a > b ? a : b)
#define BUFFER_SIZE 1024

typedef struct noeudIDTemperature
{
    int ID;
    int nbElt;
    struct noeudIDTemperature* filsG;
    struct noeudIDTemperature* filsD;
    float temperature;
    float pression;
}noeudIDTemperature;
typedef noeudIDTemperature* arbreIDTemperature;

// structure date pour récupérer la date depuis le csv
struct date
{
    int annee;
    int mois;
    int jour;
    int heure;
};

// structure coordonnées pour récupérer la lattitude et la longitude dans le csv
struct coordonnees
{
    float latitude;
    float longitude;
};

// structure station pour gérer repérer les stations 
struct station{
    int ID;
    float latitude;
    float longitude;
    float altitude;
};

// structure qui réunnis les informations pour le vent
struct vent{
    float vitesseX;
    float vitesseY;
    float vitesseMoyX;
    float vitesseMoyY;
    int nbElt;
    struct coordonnees valCoordonnees;
};

// structure
struct temp{
    struct date dateTemp;
    float temperature;
};

// structure qui permet de récupérer les données du csv
struct point
{
    int ID;
    struct date date;
    float pressionNiveauMer;
    float directionVent;
    float vitesseVent;
    float humidite;
    float pressionStation;
    float variationPression;
    float precipitations;
    struct coordonnees coordonnees;
    float temperature;
    float temperatureMin;
    float temperatureMax;
    float altitude;
    char* codeCommune;
};
typedef struct point point;

// structure d'arbre 
typedef struct noeud{
  point valeur;
  struct noeud* filsG;
  struct noeud* filsD;
}noeud;
typedef noeud* arbre;

// structure d'une pile
typedef struct ElementP{
    point valeur;
    struct ElementP* suivant;
}ElementP;
typedef ElementP* Pile;

struct temperature{
    float max;
    float min;
    float valeurs;
    float moyenne;
    int nbElt;
};

struct pression{
    float max;
    float min;
    float valeurs;
    float moyenne;
    int nbElt;
};

// structure d'un arbre d'heures 
typedef struct noeudHeure
{
    int heureTemp;
    int nbElt;
    float temperature;
    float pression;
    arbreIDTemperature ID;
    //float temperature;
    struct noeudHeure* heureFilsG;
    struct noeudHeure* heureFilsD;
}noeudHeure;
typedef noeudHeure* arbreHeure;

// structure d'un arbre de jours
typedef struct noeudJours
{
    int joursTemp;
    arbreHeure heure;
    struct noeudJours* joursFilsG;
    struct noeudJours* joursFilsD;
}noeudJours;
typedef noeudJours* arbreJours;

// structure d'un arbre de mois
typedef struct noeudMois
{
    int moisTemp;
    arbreJours jours;
    struct noeudMois* moisFilsG;
    struct noeudMois* moisFilsD;
}noeudMois;
typedef noeudMois* arbreMois;

// structure d'un arbre d'annees
typedef struct noeudAnnee
{
    int anneeTemp;
    arbreMois mois;
    struct noeudAnnee* anneeFilsG;
    struct noeudAnnee* anneeFilsD;
}noeudAnnee;
typedef noeudAnnee* arbreAnnee;

// structure d'un arbre d'idetifiants des stations
typedef struct noeudID
{
    struct station valeur;
    struct vent valeurVent;
    //struct temp valeurTemp;
    //arbreAnnee annee;
    struct temperature valeur_temperature;
    struct pression valeur_pression;
    arbre arbrePoint;
    struct noeudID* filsG;
    struct noeudID* filsD;
}noeudID;
typedef noeudID* arbreID;


struct bornes
{
    struct date dateMin;
    struct date dateMax;
    float latMax;
    float latMin;
    float longMin;
    float longMax;
};

#endif