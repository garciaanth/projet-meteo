#ifndef __ARBRE_H__
#define __ARBRE_H__
#include "struct.h"

// Entrée : un point de type point, deux sous arbres
// Sortie : un arbre
// Fonction qui créé un noeud dans un arbre
arbre creerArbrePoint(point valeur,arbre filsG,arbre filsD);

// Entrée : un entier, deux sous arbres
// Sortie : un arbre d'identifiants des stations
// Fonction qui créé un noeud dans un arbre 
arbreID creerArbreID(int valeur,arbreID filsG,arbreID filsD);

// Entrée : un arbre
// Sortie : 
// Procédure qui affiche les valeurs d'un arbre en infixe
void afficherInfixe(arbre a);

// Entrée : un arbre
// Sortie : 
// Procédure qui affiche les valeurs d'un arbre d'identifiants en infixe
void afficherInfixeID(arbreID a);

// Entrée : un arbre
// Sortie : un entier, le nombre de noeuds
// Fonction qui compte le nombre de noeuds d'un arbre 
int comptageNoeud(arbre a);

// Entrée : un arbre d'identifiants des stations, un pointeur de fichier
// Sortie : 
// Procédure qui écrit les valeurs d'un arbre d'identifiants des stations en infixe
void ecrireInfixeID(arbreID a,FILE* fluxSortie);

#endif