#ifndef __PRESSION_H__
#define __PRESSION_H__
#include "struct.h"


// Entrée : un pointeur de ficheir
// Sortie : un arbre d'années
// Fonction qui récupère les données d'un csv et les range dans la structure arbre 
arbreAnnee lectureDataSet(FILE* fluxSource);

// Entrée : un point de la structure point et un arbre d'identifiants des stations
// Sortie : un arbre d'identifiants des stations
// Fonction qui insère les valeurs de la structure points dans un arbre d'identfiants des stations
arbreID insertionArbreID(point valeur,arbreID a);

// Entrée : un point la structure point et un arbre d'années
// Sortie : un arbre d'années
// Fonction qui insère les valeurs de la structure points dans un arbre d'années
arbreAnnee insertionArbreAnnee(point valeur, arbreAnnee annee);

// Entrée : un point de la structure point et deux sous arbres de l'arbre année
// Sortie : un arbre d'années
// Fonction qui insère les valeurs de la structure points dans un arbre d'identfiants des stations
arbreAnnee creerArbreAnnee(point valeur, arbreAnnee anneeFilsG, arbreAnnee anneeFilsD);

// Entrée : un point de la structure point et un arbre de mois
// Sortie : un arbre des mois
// Fonction qui insère les valeurs de la structure points dans un arbre de mois
arbreMois insertionArbreMois(point valeur, arbreMois mois);

// Entrée : un point de la structure point et deux sous arbres de mois
// Sortie : un arbre des mois
// Fonction qui créé un noeud 
arbreMois creerArbreMois(point valeur, arbreMois moisFilsG, arbreMois moisFilsD);

// Entrée : un point de la structure point et un arbre de jours
// Sortie : un arbre des jours
// Fonction qui insère les valeurs de la structure points dans un arbre de jours
arbreJours insertionArbreJours(point valeur, arbreJours jours);

// Entrée : un point de la structure point et deux sous arbres des jours
// Sortie : un arbre de jours
// Fonction qui créé un noeud 
arbreJours creerArbreJours(point valeur, arbreJours joursFilsG, arbreJours joursFilsD);

// Entrée : un point de la structure point et un arbre d'heures
// Sortie : un arbre d'heures
// Fonction qui insère les valeurs de la structure points dans un arbre d'heures
arbreHeure insertionArbreHeure(point valeur, arbreHeure heure);

// Entrée : un point de la structure point et deux sous arbres des heures
// Sortie : un arbre des heures
// Fonction qui crée un noeud 
arbreHeure creerArbreHeure(point valeur, arbreHeure heureFilsG, arbreHeure heureFilsD);

//arbreID creerArbreID(int ID, int jours, int mois, int annee, int heure, int temperature,  arbreID filsG, arbreID filsD);
//void ecrireInfixeID(arbreID a,FILE* fluxSortie);
//void afficherInfixeID(arbreID a);

// Entrée : un point de la structure point et deux sous arbres des identifiants pour les températures
// Sortie : un arbre des identifiants
// Fonction qui crée un noeud 
arbreIDTemperature creerArbreIDTemperature(point valeur, arbreIDTemperature filsG, arbreIDTemperature filsD);

// Entrée : un point de la structure point et un sous arbres des identifiants pour les températures
// Sortie : un arbre des identifiants
// Fonction qui insères les valeurs de la structure points dans un arbre d'identifiants
arbreIDTemperature insertionArbreIDTemperature(point valeur, arbreIDTemperature ID);

// Entrée : un arbre d'identifiants, un pointeur de fichier, 4 entiers
// Sortie : 
// Procédure qui récupère les valeurs d'un arbre pour les écrires dans un fichier csv
void parcoursInfixeIDTemperature(arbreIDTemperature a, FILE* fluxSortie, int annee, int mois, int jours, int heure);

// Entrée : un arbre d'heures', un pointeur de fichier, 3 entiers
// Sortie : 
// Procédure qui récupère les valeurs d'un arbre pour les écrires dans un fichier csv
void parcoursInfixeHeure(arbreHeure a, FILE* fluxSortie, int annee, int mois, int jours);

// Entrée : un arbre de jours, un pointeur de fichier, 2 entiers
// Sortie : 
// Procédure qui récupère les valeurs d'un arbre pour les écrires dans un fichier csv
void parcoursInfixeJours(arbreJours a, FILE* fluxSortie, int annee, int mois);

// Entrée : un arbre de mois, un pointeur de fichier, 1 entiers
// Sortie : 
// Procédure qui récupère les valeurs d'un arbre pour les écrires dans un fichier csv
void parcoursInfixeMois(arbreMois a, FILE* fluxSortie, int annee);

// Entrée : un arbre d'années, un pointeur de fichier
// Sortie : 
// Procédure qui récupère les valeurs d'un arbre pour les écrires dans un fichier csv
void parcoursInfixeAnnnee(arbreAnnee a, FILE* fluxSortie);

#endif