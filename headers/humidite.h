#ifndef __HUMIDITE_H__
#define __HUMIDITE_H__
#include "struct.h"



// Entrée : un arbre
// Sortie : un entier
// Fonction qui renvoie le max d'un arbre
int maxArbre(arbre a);

// Entrée : un point de la structure point
// Sortie : un arbre
// Fonction qui supprime une valeur dans un arbre
arbre supprimerArbre(point valeur, arbre a);

// Entrée : un arbre
// Sortie : un entier
// Fonction qui renvoie le min d'un arbre
int minArbre(arbre a);

//arbre insertionArbre(point valeur,arbre a);

// Entrée : un point de la structure point et deux sous arbres
// Sortie : un arbre
// Fonction qui créé un noeud dans un arbre
arbre creerABR(point valeur,arbre filsG,arbre filsD);

// Entrée : un point de la structure point et un arbre
// Sortie : un arbre
// Fonction qui insère une valeur dans un arbre
arbre insertionAVL(point valeur, arbre a);


arbre rotationGauche(arbre b);
arbre rotationDroite(arbre a);
arbre rotationGaucheDroite(arbre a);
arbre rotationDroiteGauche(arbre a);
arbre equilibrage(arbre a);
int hauteur(arbre a);
int ecart(arbre a);

// Entrée : un pointeur de fichier
// Sortie : un arbre d'identifiants de stations
// Fonction lit un fichier csv et implémente les valeurs dans un arbre
arbreID lectureDataSet(FILE* fluxSource,struct bornes valBornes);

void parcoursLargeur(arbre a);
void afficherInfixe(arbre a);

// Entrée : un point de la structure point et un arbre d'identifiants de stations
// Sortie : un arbre d'identifiants de stations
// Fonction qui insère les valeurs de la structure point dans un arbre
arbreID insertionArbreID(point valeur,arbreID a);

// Entrée : un entier et deux sous arbres
// Sortie : un arbre d'identifiants de stations
// Fonction qui créé un noeud
arbreID creerArbreID(int valeur,arbreID filsG,arbreID filsD);

// Entrée : un arbre d'identifiants de stations
// Sortie :
// Procédure qui affiche en infixe les valeurs de l'arbre de stations
void afficherInfixeID(arbreID a);

// Entrée : un arbre
// Sortie : un entier
// Fonction qui compte le nombre de noeud dans un arbre
int comptageNoeud(arbre a);

// Entrée : un arbre d'identifiants de stations et un pointeur de fichier
// Sortie : 
// Fonction qui écrit en infixe les valeurs d'un arbre dans un fichier
void ecrireInfixeID(arbreID a,FILE* fluxSortie);
int dansLesBornes(struct bornes valBornes,point valeur);
void ecrireGNUPLOT(char* output);

#endif 