#ifndef __HAUTEUR_H__
#define __HAUTEUR_H__
#include "struct.h"

// Entrée : un pointeur de fichier
// Sortie : un arbre d'identifiants des stations
// Fonction qui récupère les valeurs d'un csv et les met dans un arbre
arbreID lectureDataSet(FILE* fluxSource,struct bornes valBornes);

// Entrée : un point de la structure point et un arbre d'identification des stations
// Sortie : un arbre d'identifiants des stations
// Fonction qui insère les valeurs de la structure point dans l'arbre
arbreID insertionArbreID(point valeur,arbreID a);

// Entrée : un entier, 3 flottants et deux sous arbres d'identifiants des stations
// Sortie : un arbre d'identifiants des stations
// Fonction qui créé un noeud dans l'arbre identification des stations
arbreID creerArbreID(int ID,float altitude, float lat, float longi,arbreID filsG,arbreID filsD);

// Entrée : un arbre d'identifiants des stations
// Sortie :
// Procédure qui affiche les valeurs d'un arbre d'identifiants des stations
void afficherInfixeID(arbreID a);

// Entrée : un arbre d'identifiants des stations et un pointeur de fichier
// Sortie :
// Procédure qui écrit les valeur d'un arbre d'identifiants des stations dans un fichier csv
void ecrireInfixeID(arbreID a,FILE* fluxSortie);
int dansLesBornes(struct bornes valBornes,point valeur);
void ecrireGNUPLOT(char* output);


#endif