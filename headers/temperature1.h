#ifndef __TEMPERATURE1_H__
#define __TEMPERATURE1_H__
#include "struct.h"

arbreID lectureDataSet(FILE* fluxSource,struct bornes valBornes);
arbre creerArbrePoint(point valeur,arbre filsG,arbre filsD);
arbreID creerArbreID(int valeur,arbreID filsG,arbreID filsD);
void afficherInfixe(arbre a);
void afficherInfixeID(arbreID a);
void ecrireInfixeID(arbreID a,FILE* fluxSortie);
arbre insertionArbre(point valeur,arbre a);
arbreID insertionArbreID(point valeur,arbreID a);
int dansLesBornes(struct bornes valBornes,point valeur);
void ecrireGNUPLOT(char* output);

#endif