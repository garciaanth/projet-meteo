#ifndef __VENT_H__
#define __VENT_H__
#include "struct.h"

// Entrée : un pointeur de fichier
// Sortie : un arbre d'identifiants des stations
// Fonction qui récupère les valeurs d'un fichier et les écrits dans un arbre
arbreID lectureDataSet(FILE* fluxSource,struct bornes valBornes);

// Entrée : un point de la structure point et un arbre d'identidiants des stations
// Sortie : un arbre d'identifiants des stations
// Fonction qui récupère les valeurs d'un point dans un arbre d'identifiants des stations
arbreID insertionArbreID(point valeur,arbreID a);

// Entrée : un entier, 4 flotants et 2 arbres d'identifiants des stations
// Sortie : un arbre d'identifiants des stations
// Fonction qui créé un noeud
arbreID creerArbreID(int ID,float vitesse, float orientation,float longitude, float latitude,arbreID filsG,arbreID filsD);

// Entrée : un arbre d'identifiants des stations
// Sortie : 
// Procédure affiches les valeurs d'un arbre d'identifiants des stations
void afficherInfixeID(arbreID a);

// Entrée : un arbre d'identification des stations et un pointeur de fichier
// Sortie : 
// Procédure qui récupère les valeurs d'un arbre et les écrit dans un fichier csv
void ecrireInfixeID(arbreID a,FILE* fluxSortie);

// Entrée : un flotant
// Sortie : un flotant
// Fonction qui transforme des degres en radians
float degreesToRad(float valeurEnDegres);
int dansLesBornes(struct bornes valBornes,point valeur);
void ecrireGNUPLOT(char* output);
#endif