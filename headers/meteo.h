#ifndef __METEO_H__
#define __METEO_H__
#include "struct.h"

// Entrée : un pointeur de chaine de caractère 
// Sortie : 
// Procédure qui lit un fichier csv
void lectureFichier(char* fichierSource);

// Entrée : un point de la structure point
// Sortie : 
// Procédure qui affiche la structure point
void afficherStruct(point input);

// Entrée : un pointeur de fichier et un point de la structure point
// Sortie : 
// Procédure qui écrit dans un fichier csv
void ecritureFichier(FILE* fluxSortie,point pointAEcrire);

// Entrée : 6 pointeurs de fichiers et 1 point de la structure point
// Sortie : 
// Procédure qui découpe le csv initial dans des csv plus petits
void decoupagePays(FILE* fluxFrance,FILE* fluxGuyane,FILE* fluxOceanIndien,FILE* fluxAntartique,FILE* fluxStPierreEtMiquelon,FILE* fluxAntilles,point pointAEcrire);

// Entrée : un pointeur de chaine de caractère 
// Sortie : une date
// Fonction qui découpe une date 
struct date decoupageDate(char* tokenEntree);

// Entrée : un pointeur de chaine de caractère 
// Sortie : des coordonnées
// Fonction qui découpe des coordonnées
struct coordonnees decoupageCoordonnees(char* tokenEntree);

void help_c();

#endif
