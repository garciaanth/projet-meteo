#ifndef __DECOUPAGE_H__
#define __DECOUPAGE_H__
#include "struct.h"

// Entrée : 6 pointeurs de fichiers, un point de la structure point
// Sortie :
// Procédure qui découpe le csv complet en csv plus pettis pour gagner du temps
void decoupagePays(FILE* fluxFrance,FILE* fluxGuyane,FILE* fluxOceanIndien,FILE* fluxAntartique,FILE* fluxStPierreEtMiquelon,FILE* fluxAntilles,point pointAEcrire);

#endif